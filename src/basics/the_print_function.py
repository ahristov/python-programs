# the_print_function.py

# The print() function

print("Hello world!")
print("This is how we print on the screen")
print()

print("This is a message on one line.", end=" ")
print("This is a message on the same line")
print("Learning Python is FUN!")