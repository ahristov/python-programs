# Python programs

This repository contains code and notes from studying Python.

## 🏠 [Homepage](https://bitbucket.org/ahristov/python-programs/src)

The code repository 🏠 is at [Homepage](https://bitbucket.org/ahristov/python-programs/src)

## Documentation pages

List of documentation pages:

* [Installation and setup](./docs/setup/)
