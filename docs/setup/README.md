# Python programs

This repository contains code and notes from studying Python.

[Home page](../../)

## Documentation: Installation and setup

### `PATH` variable on Windows

On Windows consider adding “Scripts” subdirectory from both the global and the user-specific paths.

In My case I add the following directory to the PATH environment variable:

```bash
C:\Users\Atanas Hristov\AppData\Local\Programs\Python\Python39\Scripts
C:\Users\Atanas Hristov\AppData\Roaming\Python\Python39\Scripts
```

## Working with `pip`

`Pip` is a python package installer.

It comes already preinstalled with the current Python versions.

To see the `pip` version:

```bash
λ py -m pip --version
pip 20.3.3 from C:\Users\Atanas Hristov\AppData\Local\Programs\Python\Python39\lib\site-packages\pip (python 3.9)
```

To upgrade `pyp` run:

```bash
λ py -m pip install -U pip
Requirement already satisfied: pip in c:\users\atanas hristov\appdata\local\programs\python\python39\lib\site-packages (20.3.3)
```

To get list of options, run:

```bash
py -m pip
```

To list all installed packages run:

```bash
py -m pip list
```

To install packages to _user specific location_ and not to global machine specific location, use option `--user`. Example:

```bash
pip install -U pylint --user
py -m pip install -U pylint --user
```

To upgrade a package run:

```bash
λ py -m pip install -U isort
Requirement already satisfied: isort in c:\users\atanas hristov\appdata\roaming\python\python39\site-packages (5.6.4)
```

Links:

* [Pip User Guide](https://pip.pypa.io/en/stable/user_guide)
* [Pip installation](https://pip.pypa.io/en/stable/installing/)

## Virtual environments with `venv`

The `venv` module is a lightweight way to create _virtual environment_ in a site directory.

To create new virtual environment in a subdirectory _environment_ run:

```bash
py -m venv environment
```

To activate the environment from site subdirectory _environment_ run:

```bash
.\environment\Scripts\activate
```

Links:

* [venv docu](https://docs.python.org/3/library/venv.html)
